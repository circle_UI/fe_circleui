import React from 'react'

import '@testing-library/jest-dom'
import { act } from 'react-dom/test-utils';

import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent, screen, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import ProfileUpdate from '../../profile/ProfileUpdate';

import * as api from "../../api/api"

let container = null;

let fakeUserData = {
  data: {
    user: {
      username: "frsardhafa",
      name: "faris ardhafa",
      email: "admin@ui.ac.id",
      description: "yo its me",
      faculty: "fasilkom",
      classOf: "19",
      linkedin: "linkedin.com/@frsardhf",
      twitter: "twitter.com/frsardhf",
      instagram: "instagram.com/frsardhf",
      tiktok: "tiktok.com/@frsardhf",
    }
  }
}

const constantDate = new Date('2017-06-13T04:41:20')

/*eslint no-global-assign:off*/
Date = class extends Date {
  constructor() {
    return constantDate
  }
}

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('update profile loads data', async () => {
  const fakeGetUser = jest.spyOn(api, 'apiGetCurrentUser').mockImplementation(() =>
    Promise.resolve(fakeUserData)
  );
  await act(async () => {
    render(
      <BrowserRouter>
        <ProfileUpdate>

        </ProfileUpdate>
      </BrowserRouter>

      , container
    )
  });
  expect(fakeGetUser).toHaveBeenCalled();
  expect(screen.getByText(/Update User/i)).toBeInTheDocument();
  expect(
    screen.getByTestId('input-profile-name').getAttribute("value")
  ).toEqual(fakeUserData.data.user.name);
  expect(
    screen.getByTestId('input-profile-faculty').getAttribute("value")
  ).toEqual(fakeUserData.data.user.faculty);
  expect(screen.getByText(/yo its me/i)).toBeInTheDocument();
});

it('update profile submits', async () => {
  const fakeGetUser = jest.spyOn(api, 'apiGetCurrentUser').mockImplementation(() =>
    Promise.resolve(fakeUserData)
  );
  const fakeUpdateUser = jest.spyOn(api, 'apiUpdateUser').mockImplementation(() =>
    Promise.resolve(fakeGetUser)
  );
  
  await act(async () => {
    render(
      <BrowserRouter>
        <ProfileUpdate>

        </ProfileUpdate>
      </BrowserRouter>

      , container
    )
  });

  fireEvent.change(screen.getByTestId('input-profile-name'), { target: { value: 'ardha' } })
  fireEvent.change(screen.getByTestId('input-profile-faculty'), { target: { value: 'pacil' } })
  fireEvent.change(screen.getByTestId('input-profile-classOf'), { target: { value: '2019' } })
  fireEvent.change(screen.getByTestId('input-profile-desc'), { target: { value: 'hello there' } })
  fireEvent.change(screen.getByTestId('input-profile-twitter'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-instagram'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-tiktok'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-linkedin'), { target: { value: '-' } })

  fireEvent.click(screen.getByText(/Submit/i))
  expect(fakeUpdateUser).toHaveBeenCalledWith({
    "name": "ardha",
    "description": "hello there",
    "faculty": "pacil",
    "classOf": "2019",
    "linkedin": "-",
    "twitter": "-",
    "instagram": "-",
    "tiktok": "-",
    "updatedAt": new Date()
  });
});

it('update profile submit fails', async () => {
  const fakeGetUser = jest.spyOn(api, 'apiGetCurrentUser').mockImplementation(() =>
    Promise.resolve(fakeUserData)
  );
  const fakeUpdateUser = jest.spyOn(api, 'apiUpdateUser').mockImplementation(() =>
    Promise.resolve(fakeGetUser)
  );
  const fakeUpdateUserFail = jest.spyOn(api, 'apiUpdateUser').mockImplementation(() =>
    Promise.reject({
      response: {
        data: {
          detail: 'Could not validate credentials'
        },
        status: 401,
        statusText: "Unauthorized"
      }
    })
  );
  await act(async () => {
    render(
      <BrowserRouter>
        <ProfileUpdate>

        </ProfileUpdate>
      </BrowserRouter>

      , container
    )
  });

  fireEvent.change(screen.getByTestId('input-profile-name'), { target: { value: 'ardha' } })
  fireEvent.change(screen.getByTestId('input-profile-faculty'), { target: { value: 'pacil' } })
  fireEvent.change(screen.getByTestId('input-profile-classOf'), { target: { value: '2019' } })
  fireEvent.change(screen.getByTestId('input-profile-desc'), { target: { value: 'hello there' } })
  fireEvent.change(screen.getByTestId('input-profile-twitter'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-instagram'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-tiktok'), { target: { value: '-' } })
  fireEvent.change(screen.getByTestId('input-profile-linkedin'), { target: { value: '-' } })

  fireEvent.click(screen.getByText(/Submit/i))
  await waitFor(() => {
    expect(screen.getByText(/An error has occured/i)).toBeInTheDocument()
  })
  await waitFor(() => {
    expect(screen.getByText(/Could not validate credentials/i)).toBeInTheDocument()
  })
  fireEvent.click(screen.getByLabelText('Close alert'))
  await waitFor(() => {
    expect(screen.queryByText(/An error has occured/i)).toBeNull()
  })
});
