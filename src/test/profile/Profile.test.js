import React from 'react';
import { rest } from 'msw';
import '@testing-library/jest-dom'
import { setupServer } from 'msw/node';
import { render } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom';
import Profile from '../../profile/Profile';

const url = 'http://circleui-api-dev.herokuapp.com/api/v1';

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    return res(ctx.json([]));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('read user profile', async () => {
  // Mock
  server.use(
    rest.post(`${url}/login`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'content-type': 'application/x-www-form-urlencoded'
        }),
        ctx.json({
          'username': 'faisaladisoe',
          'password': 'pplasikgaboong',
          'scope': 'me users logout'
        })
      )
    }),
    rest.get(`${url}/users/me`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'Authorization': `Bearer asdasdasdas`
        }),
      )
    })
  );

  // Arrange
  render(<BrowserRouter>
          <Profile />
         </BrowserRouter>
  );
});