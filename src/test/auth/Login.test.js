import React from 'react';
import { rest } from 'msw';
import '@testing-library/jest-dom'
import { setupServer } from 'msw/node';
import { render, fireEvent, screen } from '@testing-library/react'

import { Login } from '../../auth/Login';

const url = 'http://circleui-api-dev.herokuapp.com/api/v1';

const data = {
  'username': 'faisaladisoe',
  'password': 'pplasikgaboong',
  'scope': 'me users logout'
};

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    return res(ctx.json([]));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('user login', async () => {
  // Mock
  server.use(
    rest.post(`${url}/login`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'content-type': 'application/x-www-form-urlencoded'
        }),
        ctx.json(data)
      )
    })
  );

  // Arrange
  render(<Login />);

  // Assert
  fireEvent.change(screen.getByTestId('username'), {target: {value: 'faisaladisoe'}});
  fireEvent.change(screen.getByTestId('password'), {target: {value: 'pplasikgaboong'}});

  // Act
  fireEvent.click(screen.getByTestId('login__button'));
  await screen.findByTestId('login__button');
  expect(data).toEqual({
    'username': 'faisaladisoe',
    'password': 'pplasikgaboong',
    'scope': 'me users logout'
  });
});