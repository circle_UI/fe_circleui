import React from 'react';
import { rest } from 'msw';
import '@testing-library/jest-dom'
import { setupServer } from 'msw/node';
import { render, fireEvent, screen } from '@testing-library/react'

import { Register } from '../../auth/Register'

const url = 'http://circleui-api-dev.herokuapp.com/api/v1';

const data = {
  'username': 'faisaladisoe_testFE',
  'password': 'faisal_testFE',
  'name': 'Faisal testing',
  'email': 'faisal@ui.ac.id',
  'description': 'pplasikgaboong',
  'uiIdentityNumber': '1906787865',
  'faculty': 'Faculty of Medicine',
  'classOf': '2010',
  'linkedin': 'linkedin.com/asik',
  'twitter': 'twitter.com/yey',
  'instagram': 'instagram.com/joss',
  'tiktok': 'tiktok.com/yuhu'
}

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    return res(ctx.json([]));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('user register', async () => {
  // Mock
  server.use(
    rest.post(`${url}/register`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'content-type': 'application/json'
        }),
        ctx.json(data)
      )
    })
  );

  // Arrange
  render(<Register />);

  // Assert
  fireEvent.change(screen.getByTestId('username'), {target: {value: 'faisaladisoe_testFE'}});
  fireEvent.change(screen.getByTestId('password'), {target: {value: 'faisal_testFE'}});
  fireEvent.change(screen.getByTestId('email'), {target: {value: 'faisal@ui.ac.id'}});
  fireEvent.change(screen.getByTestId('name'), {target: {value: 'Faisal testing'}});
  fireEvent.change(screen.getByTestId('bio'), {target: {value: 'pplasikgaboong'}});
  fireEvent.change(screen.getByTestId('NPM'), {target: {value: '1906787865'}});
  fireEvent.change(screen.getByTestId('faculty'), {target: {value: 'Faculty of Medicine'}});
  fireEvent.change(screen.getByTestId('classOf'), {target: {value: '2010'}});
  fireEvent.change(screen.getByTestId('linkedin'), {target: {value: 'linkedin.com/asik'}});
  fireEvent.change(screen.getByTestId('twitter'), {target: {value: 'twitter.com/yey'}});
  fireEvent.change(screen.getByTestId('instagram'), {target: {value: 'instagram.com/joss'}});
  fireEvent.change(screen.getByTestId('tiktok'), {target: {value: 'tiktok.com/yuhu'}});

  // Act
  fireEvent.click(screen.getByTestId('login__button'));
  await screen.findByTestId('login__button');
  expect(data).toEqual({
    'username': 'faisaladisoe_testFE',
    'password': 'faisal_testFE',
    'name': 'Faisal testing',
    'email': 'faisal@ui.ac.id',
    'description': 'pplasikgaboong',
    'uiIdentityNumber': '1906787865',
    'faculty': 'Faculty of Medicine',
    'classOf': '2010',
    'linkedin': 'linkedin.com/asik',
    'twitter': 'twitter.com/yey',
    'instagram': 'instagram.com/joss',
    'tiktok': 'tiktok.com/yuhu'
  });
});