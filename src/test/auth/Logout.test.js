import React from 'react';
import qs from 'qs';
import { rest } from 'msw';
import '@testing-library/jest-dom'
import { setupServer } from 'msw/node';
import { render, fireEvent, screen } from '@testing-library/react';

import { Logout } from '../../auth/Logout';

const url = 'http://circleui-api-dev.herokuapp.com/api/v1';

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    return res(ctx.json([]));
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('user logout', async () => {
  // Mock
  server.use(
    rest.post(`${url}/login`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'content-type': 'application/x-www-form-urlencoded'
        }),
        ctx.json(qs.stringify({
          'username': 'faisaladisoe',
          'password': 'string',
          'scope': 'me users logout'
        }))
      )
    }),
    rest.get(`${url}/logout`, (req, res, ctx) => {
      return res(
        ctx.headers({
          'accept': 'application/json',
          'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        })
      )
    })
  );

  // Arrange
  render(<Logout />);

  // Act
  fireEvent.click(screen.getByTestId('login__button'));
  await screen.findByTestId('login__button');
});