/* eslint-disable testing-library/no-unnecessary-act */
import React from 'react'

import '@testing-library/jest-dom'
import { act } from 'react-dom/test-utils';
import MyCommunities from '../../my-communities/MyCommunities.js';
import MyCommunitiesCard from '../../my-communities/MyCommunitiesCard.js';

import { render, unmountComponentAtNode } from "react-dom";
import { screen } from '@testing-library/react';
import * as api from "../../api/api"


let container = null;
let fakeUserData = {
  "_id": "62260eb42393fdf12364c0e0",
  "username": "si dummy",
  "password": "$2b$12$nshAfijlr/5fGcOpqzImUO0CBEJL5h0HrIMVXLunnwG9BnQ0Pj2w.",
  "name": "dummm",
  "email": "dum@ui.ac.id",
  "description": "d",
  "uiIdentityNumber": "190000000",
  "faculty":"fasilkom",
  "classOf":"2020",
  "linkedin":"fa",
  "instagram":"fa",
  "twitter":"f",
  "tiktok":"f",
  "communityEnrolled": [
    "grup musik",
    "grup mabar",
    "grup riding"
    ],
  "eventEnrolled": [
        "a"
    ],
  "createdAt": "2022-03-07T13:43:07.716074",
  "updatedAt": "2022-03-07T13:43:07.716085"
}

let fakeUserResponse = {
    data: {
        user: {
          communityEnrolled: [
            "grup musik",
            "grup mabar",
            "grup riding"
            ]
        }
      }
}

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders my communities', async() => {
    const fakeGetUser = jest.spyOn(api, 'apiGetCurrentUser').mockImplementation(() => 
    Promise.resolve(fakeUserResponse)
  );
  await act( async() => {
    render(
        <MyCommunities/>
    , container
    )
  });
  expect(fakeGetUser).toHaveBeenCalled
});

it('renders my communities card', async() => {
  await act( async() => {
    render(
        <MyCommunitiesCard groupName={fakeUserData.communityEnrolled[0]}/>
    , container
    )
  });
  expect(screen.getByText(/grup musik/i)).toBeInTheDocument();
});


