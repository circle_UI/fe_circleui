/* eslint-disable testing-library/no-unnecessary-act */
import React from 'react'

import '@testing-library/jest-dom'
import { CommunityContext} from '../../community/CommunityContext';
import { act } from 'react-dom/test-utils';

import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent, screen, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import CommunityUpdate from '../../community/CommunityUpdate';


import * as api from "../../api/api"

let container = null;
let mockSetCommunityData = () => {}
let fakeCommunityData = {
  "_id": "0",
  "name": "UI Chess Club 2",
  "admin": "admin@ui.ac.id",
  "shortDescription": "UI Chess Club XxX",
  "longDescription": "Ayolah ribut ajaa sini",
  "status": "open",
  "rules": "Jangan berisik!!!!",
  "publicMembers": [
      "a@mail.com",
      "b@mail.com",
      "c@mail.com"
  ],
  "pendingMembers": null,
  "totalMembers": 3,
  "tags": [
      "Sport",
      "Social"
  ],
  "createdAt": "2022-03-07T13:43:07.716074",
  "updatedAt": "2022-03-07T13:43:07.716085"
}

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('update community loads data', async() => {
  await act( async() => {
    render(
      <BrowserRouter>
        <CommunityContext.Provider value={{ 
          communityData: fakeCommunityData,
        }}>
          <CommunityUpdate/>
        </CommunityContext.Provider>
      </BrowserRouter>

    , container
    )
  });
  expect(screen.getByText(/Update Community/i)).toBeInTheDocument();
  expect(
    screen.getByTestId('input-community-name').getAttribute("value")
  ).toEqual(fakeCommunityData.name);
  expect(screen.getByText(/Ayolah ribut ajaa sini/i)).toBeInTheDocument();
  expect(screen.getByText(/Jangan berisik!!!!/i)).toBeInTheDocument();
});

it('update community submits', async() => {
  const fakeUpdate = jest.spyOn(api, 'apiUpdateCommunity')
  await act( async() => {
    render(
      <BrowserRouter>
        <CommunityContext.Provider value={{ 
          communityData: fakeCommunityData,
        }}>
          <CommunityUpdate/>
        </CommunityContext.Provider>
      </BrowserRouter>

    , container
    )
  });
  
  fireEvent.change(screen.getByTestId('input-community-rules'), {target: {value: '23'}})
  fireEvent.change(screen.getByTestId('input-community-shortdesc'), {target: {value: '23'}})
  fireEvent.change(screen.getByTestId('input-community-desc'), {target: {value: '232'}})
  
  fireEvent.click(screen.getByText(/Submit/i))
  expect(fakeUpdate).toHaveBeenCalledWith(undefined, {
    "shortDescription": "23",
    "longDescription": "232",
    "status": "open",
    "rules": "23",
    "publicMembers": [
        "a@mail.com",
        "b@mail.com",
        "c@mail.com"
    ],
    "pendingMembers": null,
    "totalMembers": 3,
    "tags": [
        "Sport",
        "Social"
    ],
  });
});

it('update community submit fails', async() => {
  jest.spyOn(api, 'apiUpdateCommunity').mockImplementation(() => 
    Promise.reject({
      response: {
        data: {
          detail: 'Could not validate credentials'
        },
        status:401,
        statusText:"Unauthorized"
      }
    })
  );
  await act( async() => {
    render(
      <BrowserRouter>
        <CommunityContext.Provider value={{ 
          communityData: fakeCommunityData,
        }}>
          <CommunityUpdate/>
        </CommunityContext.Provider>
      </BrowserRouter>

    , container
    )
  });
  fireEvent.change(screen.getByTestId('input-community-rules'), {target: {value: '23'}})
  fireEvent.change(screen.getByTestId('input-community-shortdesc'), {target: {value: '23'}})
  fireEvent.change(screen.getByTestId('input-community-desc'), {target: {value: '232'}})
  fireEvent.click(screen.getByText(/Submit/i))
  await waitFor(()=>{
    expect(screen.getByText(/An error has occured/i)).toBeInTheDocument()

  })
  await waitFor(()=>{
    expect(screen.getByText(/Could not validate credentials/i)).toBeInTheDocument()
  })
  fireEvent.click(screen.getByLabelText('Close alert'))
  await waitFor(()=>{
    expect(screen.queryByText(/An error has occured/i)).toBeNull()
  })
});
