/* eslint-disable testing-library/no-unnecessary-act */
import React from 'react'

import '@testing-library/jest-dom'
import { CommunityContext} from '../../community/CommunityContext';
import { act } from 'react-dom/test-utils';
import Community from '../../community/Community';

import { render, unmountComponentAtNode } from "react-dom";
import { screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import * as api from "../../api/api"


let container = null;
let mockSetCommunityData = () => {}
let fakeCommunityData = {
  "_id": "62260eb42393fdf12364c0e0",
  "name": "UI Chess Club 2",
  "admin": "admin@ui.ac.id",
  "shortDescription": "UI Chess Club XxX",
  "longDescription": "Ayolah ribut ajaa sini",
  "status": "open",
  "rules": "Jangan berisik!!!!",
  "publicMembers": [
      "a@mail.com",
      "b@mail.com",
      "c@mail.com"
  ],
  "pendingMembers": null,
  "totalMembers": 3,
  "tags": [
      "Sport",
      "Social"
  ],
  "createdAt": "2022-03-07T13:43:07.716074",
  "updatedAt": "2022-03-07T13:43:07.716085"
}

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders community navbar and members', async() => {
  jest.spyOn(api, 'apiGetCommunityById').mockImplementation(() =>
    Promise.resolve({
      data:fakeCommunityData,
      status:200,
      statusText:"OK"
    })
  );
  await act( async() => {
    render(
      <BrowserRouter>
        <CommunityContext.Provider value={{ 
          communityData: fakeCommunityData,
          setCommunityData: mockSetCommunityData
        }}>
          <Community/>
        </CommunityContext.Provider>
      </BrowserRouter>

    , container
    )
  });
  expect(screen.getByText(/UI Chess Club 2/i)).toBeInTheDocument();
  expect(screen.getByText(/Overview/i)).toBeInTheDocument();
  expect(screen.getByText(/Announcement/i)).toBeInTheDocument();
  expect(screen.getByText(/Discussion/i)).toBeInTheDocument();
  expect(screen.getByText(/a@mail.com/i)).toBeInTheDocument();
  expect(screen.getByText(/b@mail.com/i)).toBeInTheDocument();
  expect(screen.getByText(/c@mail.com/i)).toBeInTheDocument();
});
