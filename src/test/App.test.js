import { render, screen } from '@testing-library/react';
import App from '../App';
import React from 'react'

import Explore from '../explore/Explore';

import '@testing-library/jest-dom'

test('renders navbar', () => {
  render(<App />);
  const linkElement = screen.getByText('circle.');
  expect(linkElement).toBeInTheDocument();
});

test('renders popup button', () => {
  render(<Explore />);
  const linkElement = screen.getByRole('button', {name: /popup/i})
  expect(linkElement).toBeInTheDocument();
});

test('renders Explore text', () => {
  render(<Explore />);
  const linkElement = screen.getByText(/Explore/i);
  expect(linkElement).toBeInTheDocument();
});
