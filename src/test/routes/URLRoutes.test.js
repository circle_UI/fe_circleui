import { render, screen } from '@testing-library/react';
import App from '../../App';
import React from 'react'

import '@testing-library/jest-dom'

beforeEach(() => {
  // setup a DOM element as a render target
  window.sessionStorage.clear();
});


test('renders home if logged in', () => {
  window.sessionStorage.setItem('token', JSON.stringify("eyJhbGci"));
  render(<App />);
  const linkElement = screen.getByText(/This Month's Event/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders login if not logged in', () => {
  render(<App />);
  const linkElement = screen.getByText(/Don't have an account?/i);
  expect(linkElement).toBeInTheDocument();
});