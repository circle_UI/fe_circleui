import React from 'react'
import './MyCommunitiesCard.css'

const MyCommunitiesCard = (props) => {
  if (props.showCard == "Hide") {
    return (null)
  }
    return (
      <div className='myComCard'>
          <p>{props.groupName}</p>   
      </div>
    );
  }

MyCommunitiesCard.defaultProps = {
  groupName : "Group Name",
  showCard : "Show"
}
  export default MyCommunitiesCard