import React, { useState, useEffect } from 'react'
import './MyCommunities.css'
import MyCommunitiesCard from './MyCommunitiesCard';
import {apiGetCurrentUser} from '../api/api.js'

const MyCommunities = () => {
  const [userCom, setUserCom] = useState([]);
  const getCurrentUser = () => {
    apiGetCurrentUser()
    .then((response)=> {
      console.log(response);
      const userInfo = response.data.user.communityEnrolled;
      setUserCom(userInfo);
    })
  }
  useEffect(() => {
    getCurrentUser();
  }, []);
  const cardTotal = userCom.length;
  const rowList=[];
  for(let i=0; i<cardTotal; i++){
    rowList.push(<MyCommunitiesCard groupName={userCom[i]}/>)
  }
  
  return (
    <div className='MyCommunities'>
      <h4>My Communities</h4>  
      <div className='flexContainer'>
      {rowList}
      </div>
    </div>
  )
}

export default MyCommunities
