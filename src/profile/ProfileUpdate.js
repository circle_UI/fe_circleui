import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { Form, Container, Row, Col, Alert } from 'react-bootstrap'
import './ProfileUpdate.css'
import { Link } from 'react-router-dom'
import { apiGetCurrentUser, apiUpdateUser } from '../api/api'

const ProfileUpdate = () => {
  let navigate = useNavigate()
  const [inputUsername, setInputUsername] = useState('')
  const [inputName, setInputName] = useState('')
  const [inputDescription, setInputDescription] = useState('')
  const [inputFaculty, setInputFaculty] = useState('')
  const [inputClassOf, setInputClassOf] = useState('')
  const [inputLinkedin, setInputLinkedin] = useState('')
  const [inputTwitter, setInputTwitter] = useState('')
  const [inputInstagram, setInputInstagram] = useState('')
  const [inputTikTok, setInputTikTok] = useState('')
  const [formButtonDisabled, setformButtonDisabled] = useState(false)
  const [showWarning, setshowWarning] = useState(false)
  const [warningText, setwarningText] = useState('')

  const init = async () => {
    apiGetCurrentUser().then(res => {
      console.log(res)
      setInputUsername(res.data.user.username)
      setInputName(res.data.user.name)
      setInputDescription(res.data.user.description)
      setInputFaculty(res.data.user.faculty)
      setInputClassOf(res.data.user.classOf)
      setInputLinkedin(res.data.user.linkedin)
      setInputTwitter(res.data.user.twitter)
      setInputInstagram(res.data.user.instagram)
      setInputTikTok(res.data.user.tiktok)
    })
      .catch(error => {
        console.log(error)
        if (error.response !== undefined) {
          alert(error.response.data.detail)
        }
      })
  }

  const handleNameChange = (event) => {
    setInputName(event.target.value)
  }

  const handleDescriptionChange = (event) => {
    setInputDescription(event.target.value)
  }

  const handleFacultyChange = (event) => {
    setInputFaculty(event.target.value)
  }

  const handleClassOfChange = (event) => {
    setInputClassOf(event.target.value)
  }

  const handleLinkedinChange = (event) => {
    setInputLinkedin(event.target.value)
  }

  const handleTwitterChange = (event) => {
    setInputTwitter(event.target.value)
  }

  const handleInstagramChange = (event) => {
    setInputInstagram(event.target.value)
  }

  const handleTikTokChange = (event) => {
    setInputTikTok(event.target.value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setformButtonDisabled(true)
    const data = {
        "name": inputName,
        "description": inputDescription,
        "faculty": inputFaculty,
        "classOf": inputClassOf,
        "linkedin": inputLinkedin,
        "twitter": inputTwitter,
        "instagram": inputInstagram,
        "tiktok": inputTikTok,
        "updatedAt": new Date()
      }
    apiUpdateUser(data).then((res) => {
      navigate(`/profile`)
    }).catch(error => {
      setformButtonDisabled(false)
      console.log(error.response)
      setshowWarning(true)
      if (error.response !== undefined) {
        setwarningText(error.response.data.detail)
      } else {
        setwarningText("Something went wrong")
      }
    })
  }

  useEffect(() => {
    init()
  }, [])
   
return (
<Container id="update-user">
    <div className="form-title"> 
      <p>Update User</p>
      <hr/>
    </div>
    <br/>

    {showWarning &&
      <Alert variant="danger" onClose={() => setshowWarning(false)} dismissible>
        <Alert.Heading>An error has occured!</Alert.Heading>
        <p>
          {warningText}
        </p>
      </Alert>
    }

    <Form onSubmit={handleSubmit}>
    <Row>
        <Col md="6" xs="12">
        <Form.Group className="mb-3">
          <Form.Label>Username</Form.Label>
          <Form.Control 
            value={inputUsername} 
            type="text" 
            name="username"
            required
            disabled
            >
          </Form.Control>
        </Form.Group>
        
        <Form.Group className="mb-3">
          <Form.Label>Full Name</Form.Label>
          <Form.Control 
            data-testid="input-profile-name"
            value={inputName} 
            type="text" 
            name="name"
            onChange={handleNameChange} 
            required
            >
          </Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Faculty</Form.Label>
          <Form.Control 
            data-testid="input-profile-faculty"
            value={inputFaculty} 
            type="text" 
            name="faculty"
            onChange={handleFacultyChange} 
            required
            >
          </Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Class Of</Form.Label>
          <Form.Control 
            data-testid="input-profile-classOf"
            value={inputClassOf} 
            type="text" 
            name="classOf"
            onChange={handleClassOfChange}  
            required
            >
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="formFileDisabled" className="mb-3">
          <Form.Label>Profile Picture</Form.Label>
          <Form.Control type="file" disabled />
        </Form.Group>

      </Col>
      <Col md="6" xs="12">

        <Form.Group className="mb-3">
          <Form.Label>Description</Form.Label>
          <Form.Control 
             data-testid="input-profile-desc"  
             as="textarea" 
             value={inputDescription} 
             type="text" 
             name="description" 
             onChange={handleDescriptionChange} 
             required></Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Social Media</Form.Label>
          <Form.Control 
            data-testid="input-profile-twitter"
            value={inputTwitter} 
            type="text" 
            name="twitter"
            placeholder="https://www.twitter.com/"
            onChange={handleTwitterChange} 
            required
            >
          </Form.Control>

          <Form.Control 
            data-testid="input-profile-instagram"
            value={inputInstagram} 
            type="text" 
            name="instagram"
            placeholder="https://www.instagram.com/"
            onChange={handleInstagramChange} 
            required
            >
          </Form.Control>

          <Form.Control 
            data-testid="input-profile-tiktok"
            value={inputTikTok} 
            type="text" 
            name="tiktok"
            placeholder="https://www.tiktok.com/@"
            onChange={handleTikTokChange} 
            required
            >
          </Form.Control>

          <Form.Control 
            data-testid="input-profile-linkedin"
            value={inputLinkedin} 
            type="text" 
            name="linkedin"
            placeholder="https://www.linkedin.com/@"
            onChange={handleLinkedinChange} 
            required
            >
          </Form.Control>
        </Form.Group>
      </Col>
      
      <div className="form-buttons">
          <Link to={`/profile`} disabled={formButtonDisabled ? true : false}>
            <button className='button-secondary'>
              Cancel
            </button>
          </Link>
          <button className='button-primary' type="submit" disabled={formButtonDisabled ? true : false}>
            Submit
          </button>
        </div>
    </Row>
  </Form>


  
</Container>
)

}

export default ProfileUpdate