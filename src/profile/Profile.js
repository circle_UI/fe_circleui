import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './Profile.css'
import { Token } from '../auth/Token'
import Avatar from '../img/avatar.jpg'
import { ReactComponent as TikTokLogo } from '../img/tiktok.svg'
import { ReactComponent as TwitterLogo } from '../img/twitter.svg'
import { ReactComponent as InstagramLogo } from '../img/instagram.svg'
import { ReactComponent as LinkedInLogo } from '../img/linkedin.svg'
import { ReactComponent as SettingIcon } from '../img/setting.svg'

const userProfile = async data => {
  return await axios(data);
}

const Profile = () => {
  const [profile, setProfile] = useState();
  const { token } = Token();

  useEffect(() => {
    const url = 'https://circleui-api-dev.herokuapp.com/api/v1/users/me';
    const options = {
      method: 'GET',
      headers: {
        'accept': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      url: url
    };
    userProfile(options)
      .then(res => {
        setProfile(res.data.user);
      });
  }, [token]);
  
  return (
    <div id="profile">
      <Container>
        <Row>
          <Col>
          </Col>
          <Col xs="12" md="10">
            <Row className='row-align'>
              <Col xs="6" md="5" lg="4" xl="4" className='col-padding-1-color justify-content-center'>
                <div className='avatar-component'>
                  <img src={Avatar} className='avatar' alt='user profile'></img>
                </div>
                <Row>
                  <Col className='col-padding-0'>
                  </Col>
                  <Col className='col-padding-0'>
                    <a href={profile && profile.tiktok} target="_blank" rel="noopener noreferrer">
                      <TikTokLogo className='tiktok' />
                    </a>
                  </Col>
                  <Col className='col-padding-0'>
                    <a href={profile && profile.twitter} target="_blank" rel="noopener noreferrer">
                      <TwitterLogo className='twitter' />
                    </a>
                  </Col>
                  <Col className='col-padding-0'>
                    <a href={profile && profile.instagram} target="_blank" rel="noopener noreferrer">
                      <InstagramLogo className='instagram' />
                    </a>
                  </Col>
                  <Col className='col-padding-0'>
                    <a href={profile && profile.linkedin} target="_blank" rel="noopener noreferrer">
                      <LinkedInLogo className='linkedin' />
                    </a>
                  </Col>
                </Row>
              </Col>
              <Col className='col-padding-2-color'>
                <div className='profile-upper-component'>
                  <div className='profile-upper-component-child'>
                    {profile && profile.username}
                  </div>
                </div>
                <div className='profile-lower-component'>
                  <div className='profile-lower-component-child'>
                    Full Name: {profile && profile.name}
                  </div>
                  <div className='profile-lower-component-child'>
                    Faculty: {profile && profile.faculty}
                  </div>
                  <div className='profile-lower-component-child'>
                    Class Of: {profile && profile.classOf}
                  </div>
                  <div className='profile-lower-component-child'>
                    Bio: {profile && profile.description}
                  </div>
                </div>
              </Col>
              <Col className='col-padding-3-color justify-content-end'>
                <div className='profile-upper-component-child-2'>
                  <Link to={`/profile/update`}>
                    <SettingIcon className='setting' />
                  </Link>
                </div>
              </Col>
            </Row>
          </Col>
          <Col>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Profile