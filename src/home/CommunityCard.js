import React from 'react'
import './CommunityCard.css'

const CommunityCard = (props) => {
  if (props.showCard == "Hide") {
    return (null)
  }
    return (
      <div className='comCard'>
        <p>{props.groupName}</p>   
      </div>
    );
  }

CommunityCard.defaultProps = {
  groupName : "Group Name",
  showCard : "Show"
}
  export default CommunityCard