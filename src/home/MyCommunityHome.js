import React, { useState, useEffect } from 'react'
import axios from 'axios';
import './MyCommunityHome.css'
import CommunityCard from './CommunityCard';
import {apiGetCurrentUser} from '../api/api.js'


const MyCommunityHome = () => {
  const [userCom, setUserCom] = useState([]);
  const getCurrentUser = () => {
    apiGetCurrentUser()
    .then((response)=> {
      const userInfo = response.data.user.communityEnrolled;
      setUserCom(userInfo);
      console.log(userInfo);
    })
  }
  useEffect(() => {
    getCurrentUser();
  }, []);
  
  const totalCard = userCom.length;
    return (
      <div className="MyComHome">
        <p>My Community</p>
        <p className="viewAll">
            <a href="/my-communities">View All{'>'}</a>
        </p>
        <div className='cardRow'>
        {totalCard > 0 ? <CommunityCard groupName={userCom[0]}/> : <CommunityCard showCard="Hide"/>}
        {totalCard > 1 ? <CommunityCard groupName={userCom[1]}/> : <CommunityCard showCard="Hide"/>}
        {totalCard > 2 ? <CommunityCard groupName={userCom[2]}/> : <CommunityCard showCard="Hide"/>}
        {totalCard > 3 ? <CommunityCard groupName={userCom[3]}/> : <CommunityCard showCard="Hide"/>}
        </div>
      </div>
    );
  }
  
  export default MyCommunityHome