import React, { useEffect, useState } from 'react'
import EventBanner from './EventBanner';
import MyCommunityHome from './MyCommunityHome';
import "./Home.css"

const Home = () => {
  
  return (
    <div className="Home">
     <EventBanner />
     <MyCommunityHome />
    </div>
  );
}

export default Home