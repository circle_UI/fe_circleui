import React, { useState } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Explore from '../explore/Explore';
import Home from '../home/Home';
import Community from '../community/Community';
import CommunityCreate from '../community/CommunityCreate';
import Event from '../event/Event';
import Profile from '../profile/Profile';
import UpdateProfile from '../profile/ProfileUpdate';
import MyCommunities from '../my-communities/MyCommunities';
import MyEvents from '../my-events/MyEvents';
import CommunitySettings from '../community/CommunitySettings';
import { Login } from '../auth/Login';
import { Register } from '../auth/Register';
import { Logout } from '../auth/Logout'

const URLRoutes = () => {
  if (sessionStorage.getItem('token') !== null){
    console.log("logged in")
    return(
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route path="/explore" element={<Explore/>} />
          <Route path="/my-communities" element={<MyCommunities/>}/>
          {/*<Route path="/my-events" element={<MyEvents/>}/> */}
          <Route path="/community/create" element={<CommunityCreate/>} />
          <Route path="/community/:id/settings" element={<CommunitySettings/>}/>
          <Route path="/community/:id/*" element={<Community/>} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/profile' element={<Profile />} />
          <Route path='/profile/update' element={<UpdateProfile />} />
          {/* <Route path="/event/:id" element={<Event/>} />*/}
          <Route path='*' element={<Navigate to="/" />} />
        </Routes>
      </BrowserRouter>
    )
  } else {
    console.log("not logged in")
      return(
      <BrowserRouter>
        <Routes>
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='*' element={<Navigate to="/login" />}/>
        </Routes>
      </BrowserRouter>
    )
  }




}

export default URLRoutes
