import React  from "react"
import { Navbar, Container, Nav } from "react-bootstrap";
import './Navbar.css'
import { Token } from '../auth/Token';

const MainNavbar = () => {
  const { token } = Token();
  return (
    <Navbar bg="dark" variant="dark" expand="lg" id="navbar-main">
        <Container>
            <Navbar.Brand href="/" id="navbar-title">
              <span id="nav-text-circle">circle.</span><span id="nav-text-ui">ui</span>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              {token && 
                <>
                  <Nav.Link href="/">Home</Nav.Link>
                  <Nav.Link href="/explore">Explore</Nav.Link>
                  <Nav.Link href="/my-communities">My Communities</Nav.Link>
                  <Nav.Link href="/my-events">My Events</Nav.Link>
                  <Nav.Link href='/profile'>Profile</Nav.Link>
                </>
              }
              {!token ? <Nav.Link href='/login'>Login</Nav.Link> : <Nav.Link href='/logout'>Logout</Nav.Link>}
            </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
  )
}

export default MainNavbar