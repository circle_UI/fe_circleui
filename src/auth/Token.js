import { useState } from 'react';

export const Token = () => {
  const getToken = () => {
    const tokenString = sessionStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    return userToken
  };
  
  const [token, setToken] = useState(getToken());

  const storeToken = userToken => {
    sessionStorage.setItem('token', JSON.stringify(userToken));
    setToken(userToken);
  };

  return {
    setToken: storeToken,
    token
  }
}
