import React, { useState } from 'react';

import qs from 'qs';
import axios from 'axios';
import { Navigate } from 'react-router-dom';

import './Login.css';
import { Token } from './Token';

const loginUser = async data => {
  return axios(data);
}

export const Login = () => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const { token, setToken } = Token();

  // if (token) {
  //   return <Navigate to='/logout' />;
  // }

  const handleSubmit = async e => {
    e.preventDefault();
    const url = 'https://circleui-api-dev.herokuapp.com/api/v1/login'
    const data = {
      'username': username,
      'password': password,
      'scope': 'me users logout'
    }
    const options = {
      method: 'POST',
      headers: {
        'accept': 'application/json',
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: qs.stringify(data),
      url: url
    };
    const result = await loginUser(options);
    setToken(result.data.access_token);
    window.location.reload();
  }

  return (
    <div className='auth__container'>
      <div className='title__container'>
        <h1 id='login__title'>Circle UI</h1>
      </div>
      <div className='form__container'>
        <form onSubmit = {handleSubmit} className='form__login'>
          <input
            type="text"
            data-testid='username'
            name='username'
            placeholder='username'
            autoComplete='off'
            onChange = {e => setUsername(e.target.value)}
            required 
          />
          <input
            type="password"
            data-testid="password"
            name="password"
            placeholder='password'
            onChange = {e => setPassword(e.target.value)}
            required 
          />
          <input type="submit" data-testid='login__button' id='login_button' value="Login" />
        </form>
      </div>
      <div className='redirect__container'>
        <p>Don't have an account? <a href='/register'>Sign up</a></p>
      </div>
    </div>
  )
}
