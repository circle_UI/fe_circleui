import React from 'react';
import axios from 'axios';

import './Login.css';
import { Login } from './Login';
import { Token } from './Token';

const logoutUser = async data => {
  return axios(data);
}

export const Logout = () => {
  const { token } = Token();
  if (!token) {
    return <Login />;
  }

  const handleSubmit = async e => {
    e.preventDefault();
    const url = 'https://circleui-api-dev.herokuapp.com/api/v1/logout';
    const options = {
      method: 'GET',
      headers: {
        'accept': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      url: url
    };
    await logoutUser(options);
    sessionStorage.removeItem('token');
    window.location.reload();
  }

  return (
    <div className='auth__container'>
      <div>
        <p>Are you sure want to logout?</p>
      </div>
      <div>
        <form onSubmit = {handleSubmit}>
          <input data-testid="logout__button" type="submit" value="Logout" />
        </form>
      </div>
    </div>
  );
}
