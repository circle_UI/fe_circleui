import React, { useState, useEffect } from 'react';

import axios from 'axios';
import { Navigate } from 'react-router-dom';

import './Login.css'

const registerUser = async data => {
  return axios(data);
}

export const Register = () => {
  const [status, setStatus] = useState();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [email, setEmail] = useState();
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [NPM, setNPM] = useState();
  const [faculty, setFaculty] = useState();
  const [classOf, setClassOf] = useState();
  const [linkedin, setLinkedin] = useState();
  const [twitter, setTwitter] = useState();
  const [instagram, setInstagram] = useState();
  const [tiktok, setTiktok] = useState();

  useEffect(() => {
    if (status === 'accepted') {
      return <Navigate to='/login' />;
    }
  }, [status])

  const handleSubmit = async e => {
    e.preventDefault();
    const url = 'https://circleui-api-dev.herokuapp.com/api/v1/register'
    const data = {
      'username': username,
      'password': password,
      'name': name,
      'email': email,
      'description': description,
      'uiIdentityNumber': NPM,
      'faculty': faculty,
      'classOf': classOf,
      'linkedin': linkedin,
      'twitter': twitter,
      'instagram': instagram,
      'tiktok': tiktok
    }
    const options = {
      method: 'POST',
      headers: {
        'accept': 'application/json',
        'content-type': 'application/json'
      },
      data: JSON.stringify(data),
      url: url
    };
    const result = await registerUser(options);
    setStatus(result.data.status);
  }

  return (
    <div className='auth__container'>
      <div className='title__container'>
        <h1 id='login__title'>Circle UI Registration Account</h1>
      </div>
      <div className='form__container'>
        <form onSubmit = {handleSubmit} className='form__login'>
          <input
            type="text"
            data-testid='username'
            name='username'
            placeholder='username'
            autoComplete='off'
            onChange = {e => setUsername(e.target.value)}
            required 
          />
          <input
            type="password"
            data-testid="password"
            name="password"
            placeholder='password'
            onChange = {e => setPassword(e.target.value)}
            required 
          />
          <input
            type="email"
            data-testid="email"
            name="email"
            placeholder='email'
            autoComplete='off'
            onChange = {e => setEmail(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="name"
            name="name"
            placeholder="name"
            autoComplete='off'
            onChange = {e => setName(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="bio"
            name="bio"
            placeholder="bio"
            autoComplete='off'
            onChange = {e => setDescription(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="NPM"
            name="NPM"
            placeholder="NPM"
            autoComplete='off'
            onChange = {e => setNPM(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="faculty"
            name="faculty"
            placeholder="faculty"
            autoComplete='off'
            onChange = {e => setFaculty(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="classOf"
            name="classOf"
            placeholder="classOf"
            autoComplete='off'
            onChange = {e => setClassOf(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="linkedin"
            name="linkedin"
            placeholder="linkedin"
            autoComplete='off'
            onChange = {e => setLinkedin(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="twitter"
            name="twitter"
            placeholder="twitter"
            autoComplete='off'
            onChange = {e => setTwitter(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="instagram"
            name="instagram"
            placeholder="instagram"
            autoComplete='off'
            onChange = {e => setInstagram(e.target.value)}
            required 
          />
          <input
            type="text"
            data-testid="tiktok"
            name="tiktok"
            placeholder="tiktok"
            autoComplete='off'
            onChange = {e => setTiktok(e.target.value)}
            required 
          />
          <input type="submit" data-testid='login__button' id='login_button' value="Register" />
        </form>
      </div>
      <div className='redirect__container'>
        <p>Already have an account? <a href='/login'>Login</a></p>
      </div>
    </div>
  )
}
