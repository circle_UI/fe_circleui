import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import { Form, Container, Row, Col, Alert } from 'react-bootstrap'
import makeAnimated from 'react-select/animated'
import Select from 'react-select'
import { Link } from "react-router-dom";
import './CommunityCreate.css'
import { apiGetCurrentUser, apiPostCommunity } from '../api/api'


const CommunityCreate = () => {
  const animatedComponents = makeAnimated()
  let navigate = useNavigate()
  const [showWarning, setshowWarning] = useState(false)
  const [warningText, setwarningText] = useState('')
  const [formButtonDisabled, setformButtonDisabled] = useState(true)
  const [adminEmail, setadminEmail] = useState('')
  const [inputName, setInputName] = useState('')
  const [inputTags, setInputTags] = useState([])
  const [inputRules, setInputRules] = useState('')
  const [inputShortDesc, setInputShortDesc] = useState('')
  const [inputDesc, setInputDesc] = useState('')
  const default_status = "open"

  const tags = [
    { value: 'Sport', label: 'Sport' },
    { value: 'Hobby', label: 'Hobby' },
    { value: 'Social', label: 'Social' },
    { value: 'Art', label: 'Art'}
  ]

  const handleNameChange = (event) => {
    setInputName(event.target.value)
  }

  const handleRulesChange = (event) => {
    setInputRules(event.target.value)
  }

  const handleShortDescChange = (event) => {
    setInputShortDesc(event.target.value)
  }

  const handleDescChange = (event) => {
    setInputDesc(event.target.value)
  }

  const handleTagsChange = (event) => {
    setInputTags(Array.isArray(event) ? event.map(x => x.value) : [])
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    setformButtonDisabled(true)
    let data = {
      name: inputName,
      admin: adminEmail,
      tags: inputTags,
      rules: inputRules,
      shortDescription: inputShortDesc,
      longDescription: inputDesc,
      status: default_status
    }
    apiPostCommunity(data).then((res) => {
      navigate(`/community/${res.data._id}/overview`)
    }).catch(error => {
      setformButtonDisabled(false)
      console.log(error.response)
      setshowWarning(true)
      if (error.response !== undefined){
        setwarningText(error.response.data.detail)
      } else {
        setwarningText("Something went wrong")
      }
    })
  }

  const init = async () => {
    apiGetCurrentUser().then(res =>{
      console.log(res)
      setadminEmail(res.data.user.email)
      setformButtonDisabled(false)
    }).catch(error => {
      navigate('/')
      console.log(error.response)
    })
    
  }

useEffect(() => {
    init()
}, [])


return (
<Container id="create-community">
    <div className="form-title"> 
      <p>Create Community</p>
      <hr/>
    </div>
    <br/>

    {showWarning && 
      <Alert variant="danger" onClose={() => setshowWarning(false)} dismissible>
        <Alert.Heading>An error has occured!</Alert.Heading>
        <p>
          {warningText}
        </p>
      </Alert>
    }

    <Form onSubmit={handleSubmit}>
    <Row>
        <Col md="6" xs="12">
        <Form.Group className="mb-3">
          <Form.Label>Name</Form.Label>
          <Form.Control data-testid="input-community-name" value={inputName} type="text" name="name" onChange={handleNameChange} maxLength="50" required></Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Tags</Form.Label>
          <Select
                name="tags"
                data-testid="input-community-tags"
                isMulti
                className="basic-multi-select"
                classNamePrefix="select"
                closeMenuOnSelect={false}
                components={animatedComponents}
                options={tags}
                onChange={handleTagsChange}
                required
            />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Rules</Form.Label>
          <Form.Control data-testid="input-community-rules" as="textarea" value={inputRules} type="text" name="rules" onChange={handleRulesChange} maxLength="300" required></Form.Control>
        </Form.Group>

        <Form.Group controlId="formFileDisabled" className="mb-3">
          <Form.Label>Image</Form.Label>
          <Form.Control type="file" disabled />
        </Form.Group>
      </Col>
      <Col md="6" xs="12">
        <Form.Group className="mb-3">
          <Form.Label>Short Description</Form.Label>
          <Form.Control data-testid="input-community-shortdesc" as="textarea" value={inputShortDesc} type="text" name="shortDesc" onChange={handleShortDescChange} maxLength="100" required></Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Description</Form.Label>
          <Form.Control data-testid="input-community-desc" as="textarea" value={inputDesc} type="text" name="longDesc" onChange={handleDescChange} maxLength="255" required></Form.Control>
        </Form.Group>
      </Col>
      <div className="form-buttons">
          <Link to="/explore">
          <button className='button-secondary' disabled={formButtonDisabled ? true: false}>
            Cancel
          </button>
          </Link>
          <button className='button-primary' type="submit" disabled={formButtonDisabled ? true: false}>
            Submit
          </button>
        </div>
    </Row>

  </Form>


  
</Container>
)

}
export default CommunityCreate

