import React, { useState, useEffect, useContext } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Form, Container, Row, Col, Alert } from 'react-bootstrap'
import makeAnimated from 'react-select/animated'
import Select from 'react-select'
import './CommunityUpdate.css'
import { Link } from 'react-router-dom'
import { CommunityContext } from './CommunityContext'
import { apiUpdateCommunity } from '../api/api'


const CommunityUpdate = () => {
  const { communityData } = useContext(CommunityContext)
  let { id } = useParams('0')
  let navigate = useNavigate()
  const animatedComponents = makeAnimated()
  const [showWarning, setshowWarning] = useState(false)
  const [warningText, setwarningText] = useState('')
  const [formButtonDisabled, setformButtonDisabled] = useState(false)
  const [inputName, setInputName] = useState('')
  const [inputTags, setInputTags] = useState([])
  const [inputRules, setInputRules] = useState('')
  const [inputShortDesc, setInputShortDesc] = useState('')
  const [inputDesc, setInputDesc] = useState('')

  const tags = [
    { value: 'Sport', label: 'Sport' },
    { value: 'Hobby', label: 'Hobby' },
    { value: 'Social', label: 'Social' },
    { value: 'Art', label: 'Art'}
  ]

  const handleRulesChange = (event) => {
    setInputRules(event.target.value)
  }

  const handleShortDescChange = (event) => {
    setInputShortDesc(event.target.value)
  }

  const handleDescChange = (event) => {
    setInputDesc(event.target.value)
  }

  const handleTagsChange = (event) => {
    setInputTags(Array.isArray(event) ? event.map(x => x.value) : [])
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setformButtonDisabled(true)
    let data = {
      "shortDescription": inputShortDesc,
      "longDescription": inputDesc,
      "status": communityData.status,
      "rules": inputRules,
      "publicMembers": communityData.publicMembers,
      "pendingMembers": communityData.pendingMembers,
      "totalMembers": communityData.totalMembers,
      "tags": inputTags
    }
    apiUpdateCommunity(id, data).then((res) => {
      navigate(`/community/${res.data._id}/overview`)
    }).catch(error => {
      setformButtonDisabled(false)
      console.log(error.response)
      setshowWarning(true)
      if (error.response !== undefined){
        setwarningText(error.response.data.detail)
      } else {
        setwarningText("Something went wrong")
      }
    })
  }

  const init = async () => {
    setInputName(communityData.name)
    setInputTags(communityData.tags)
    setInputRules(communityData.rules)
    setInputShortDesc(communityData.shortDescription)
    setInputDesc(communityData.longDescription)
  }

useEffect(() => {
    init()
    console.log(JSON.parse(sessionStorage.getItem('token')))
}, [])


return (
<Container id="create-community">
    <div className="form-title"> 
      <p>Update Community</p>
      <hr/>
    </div>
    <br/>
    {showWarning && 
      <Alert variant="danger" onClose={() => setshowWarning(false)} dismissible>
        <Alert.Heading>An error has occured!</Alert.Heading>
        <p>
          {warningText}
        </p>
      </Alert>
    }

    <Form onSubmit={handleSubmit}>
    <Row>
        <Col md="6" xs="12">
        <Form.Group className="mb-3">
          <Form.Label>Name</Form.Label>
          <Form.Control 
            value={inputName} 
            type="text" 
            name="name"
            data-testid="input-community-name" 
            required
            disabled
            maxLength="50"
            >
          </Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Tags</Form.Label>
          <Select
                name="tags"
                data-testid="input-community-tags" 
                isMulti
                className="basic-multi-select"
                classNamePrefix="select"
                closeMenuOnSelect={false}
                components={animatedComponents}
                options={tags}
                value={tags.filter(obj => inputTags.includes(obj.value))}
                onChange={handleTagsChange}
                required
            />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Rules</Form.Label>
          <Form.Control 
            data-testid="input-community-rules" 
            as="textarea" 
            value={inputRules} 
            type="text" 
            name="rules" 
            onChange={handleRulesChange} 
            maxLength="300"
            required
          >
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="formFileDisabled" className="mb-3">
          <Form.Label>Image</Form.Label>
          <Form.Control type="file" disabled />
        </Form.Group>
      </Col>
      <Col md="6" xs="12">
        <Form.Group className="mb-3">
          <Form.Label>Short Description</Form.Label>
          <Form.Control 
            data-testid="input-community-shortdesc" 
            as="textarea" 
            value={inputShortDesc} 
            type="text" 
            name="shortDesc" 
            onChange={handleShortDescChange} 
            maxLength="100"
            required
          >
          </Form.Control>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Description</Form.Label>
          <Form.Control 
            data-testid="input-community-desc"  
            as="textarea" 
            value={inputDesc} 
            type="text" 
            name="longDesc" 
            onChange={handleDescChange} 
            maxLength="255"
            required
          >
          </Form.Control>
        </Form.Group>
      </Col>
      <div className="form-buttons">
          <Link to={`/community/${id}/overview`} disabled={formButtonDisabled ? true: false}>
            <button className='button-secondary'>
              Cancel
            </button>
          </Link>
          <button className='button-primary' type="submit" disabled={formButtonDisabled ? true: false}>
            Submit
          </button>
        </div>
    </Row>
  </Form>


  
</Container>
)

}

export default CommunityUpdate