import React, { useState, createContext } from "react";

export const CommunityContext = createContext()

export const CommunityProvider = (props) => {
    const [ communityData, setCommunityData ] = useState(
        {
            "_id": "",
            "name": "Loading....",
            "admin": "",
            "shortDescription": "",
            "longDescription": "",
            "status": "",
            "rules": "",
            "publicMembers": [],
            "pendingMembers": [],
            "totalMembers": 1,
            "tags": [],
            "createdAt": "",
            "updatedAt": ""
        }
    )

    const data = {
      communityData,
      setCommunityData
    }

    return (
        <CommunityContext.Provider value={data}>
            {props.children}
        </CommunityContext.Provider>
    )
}
