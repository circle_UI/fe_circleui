import React, { useContext, useEffect } from 'react'
import { Routes, Route, useParams, useNavigate, Navigate } from 'react-router-dom'
import CommunityAnnouncement from './CommunityAnnouncement'
import CommunityOverview from './CommunityOverview'
import CommunityRoom from './CommunityRoom'
import CommunitySettings from './CommunitySettings'
import "./Community.css"
import CommunityNavbar from './CommunityNavbar'
import { apiGetCommunityById } from '../api/api'
import { CommunityContext } from './CommunityContext'
import { Col, Container, Row } from 'react-bootstrap'

const Community = () => {
  const { communityData, setCommunityData } = useContext(CommunityContext)
  let { id } = useParams()
  let navigate = useNavigate()

  const init = async (id) => {
    apiGetCommunityById(id).then(res => {
      console.log(res)
      let communityData = res.data
      let pm = communityData.publicMembers
      communityData.publicMembers = pm ? pm : []
      setCommunityData(res.data)
      pm = communityData.pendingMembers
      communityData.pendingMembers = pm ? pm : []
    })
    .catch(error => {
      console.log(error)
      if (error.response !== undefined){
        alert(error.response.data.detail)
      }
      navigate('/')
    })
  }

  useEffect(() => {
    init(id)
  }, [])
  
  return (
    <div id="community">
      <CommunityNavbar/>
      <Container>
        <Row>
          <Col xs="12" lg="9">
            <Routes>
              <Route path="overview" element={<CommunityOverview/>}/>
              <Route path='*' element={<Navigate to="overview" />} />
              {/* TODO */}
              {/* <Route path="announcement" element={<CommunityAnnouncement/>} />
              <Route path="discussion" element={<CommunityRoom/>}/> */}
            </Routes>
          </Col>
          <Col xs="12" lg="3">
            <div className='overview-component member-list'>
            <p className='title'>Members</p>
            <div className="member-card">{communityData.admin}</div>
            {
              communityData.publicMembers.map((item, index)=>(
                <div className="member-card" key={index}>{item}</div>
              ))
            }
          </div>
          </Col>
        </Row>
      </Container>
    </div>

  )
}

export default Community