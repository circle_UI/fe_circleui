import React, { useContext } from 'react'
import { Container, Row, Col } from "react-bootstrap";
import { CommunityContext } from './CommunityContext';
import './CommunityOverview.css'

const CommunityOverview = () => {
  const { communityData } = useContext(CommunityContext)
  return (
    <div id="community-overview">
      <div className='overview-component'>
        <p className='title'>About Us</p>
        <p>{communityData.longDescription}</p>
      </div>
      <div className='overview-component'>
        <p className='title'>Rules</p>
        <p>{communityData.rules}</p>
      </div>
    </div>
  )
}

export default CommunityOverview
