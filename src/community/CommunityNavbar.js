import React, { useContext } from 'react'
import { Link, NavLink } from "react-router-dom";
import { Container } from "react-bootstrap";

import SettingsIcon from '@material-ui/icons/Settings'
import { CommunityContext } from './CommunityContext';

const CommunityNavbar = () => {
  const { communityData } = useContext(CommunityContext)
  let currentPath = window.location.pathname
  return (
    <div id="community-header">
      <div id="community-image"></div>
      <div id="community-info">
        <Container>
        <p id="title">{communityData.name}</p>
        <p id="member-count">{communityData.totalMembers} Members</p>
        </Container>
      </div>
      <div id="community-navbar">
        <Container>
        <NavLink to="overview" className={currentPath === "/overview" ? "active" : ""}>
          <div>Overview</div>
        </NavLink>
        <NavLink to="announcement" className={currentPath === "/announcement" ? "active" : ""}>
          <div>Announcement</div>
        </NavLink>
        <NavLink to="discussion" className={currentPath === "/discussion" ? "active" : ""}>
          <div>Discussion</div>
        </NavLink>
        <Link to="settings"><SettingsIcon id="settings"></SettingsIcon></Link>
        </Container>

      </div>
    </div>
  )
}

export default CommunityNavbar