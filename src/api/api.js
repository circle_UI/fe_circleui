import axios from 'axios';

export const axiosClient  =  axios.create({
  baseURL: `https://circleui-api-dev.herokuapp.com/api/v1`,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': sessionStorage.getItem('token') === undefined ?  null : `Bearer ${JSON.parse(sessionStorage.getItem('token'))}`
  }
})

export const apiPostCommunity = (data) => {
  return axiosClient.post(`/community`, data)
}

export const apiUpdateCommunity = (id, data) => {
  axiosClient.interceptors.request.use(request => {
    console.log('Starting Request', JSON.stringify(request, null, 2))
    return request
  })
  return axiosClient.put(`/community/${id}`, data)
}

export const apiGetCommunityById = (id) => {
  axiosClient.interceptors.request.use(request => {
    console.log('Starting Request', JSON.stringify(request, null, 2))
    return request
  })
  
  axiosClient.interceptors.response.use(response => {
    console.log('Response:', JSON.stringify(response, null, 2))
    return response
  })
  return axiosClient.get(`/community/${id}`)
}

export const apiGetAllCommunities = () => {
  return axiosClient.get(`/community/`)
}

export const apiGetAllUsers = () => {
  return axiosClient.get(`/users?skip=0&limit=0/`)
}

export const apiUpdateUser = (data) => {
  return axiosClient.put(`/users/me/update`, data)
}
  
export const apiGetCurrentUser = () => {
  return axiosClient.get(`/users/me`)
}