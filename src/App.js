import React from "react"
import './App.css';
import './colors.css'
import 'bootstrap/dist/css/bootstrap.css';
import URLRoutes from "./routes/URLRoutes";
import MainNavbar from './navbar/MainNavbar'
import { CommunityProvider } from "./community/CommunityContext";

function App() {
  return(
    <>
      <CommunityProvider>
        <MainNavbar/>
        <URLRoutes/>
      </CommunityProvider>
    </>
  )
}

export default App;
